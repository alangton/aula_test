# Aula Code Test Phase 1

This fires up a very small rest service that will seed a local db (either in memory or in MongoDB) with minimal song data.  

There are currently only two REST endpoints, the rest haven't been implemented:

* `/api/songs`        --> fetches all the songs (minimal pagination, three at a time, five total songs.)
* `/api/songs/:id`    --> fetch a single song by its id

(From here on out, I'm assuming you're using yarn, not npm.)

Once cloned to your system, run `yarn` to set up the packages.

There are the following scripts to try things out:

* `yarn start` --> Will start the service with an in-memory db.
* `yarn curl` --> Will run a curl command to get the full list of songs.
* `yarn web` --> Will launch a browser (if on Mac) with the list of songs.

There's minimal pagination (there's only five songs in the /files/music folder), but you can try things like:

* `http://localhost:3000/api/songs?page=2` --> Go to page two of the data
* `http://localhost:3000/api/songs?limit=10` --> See 10 items per page (default is 3)

### Mongo Setup
There's also a docker-compose.yml setup that will build the node application and fire it up with a MongoDB container.

* Install Docker on Mac: https://docs.docker.com/docker-for-mac/install/
* Install Docker on Windows

With Docker present, run:

* `docker-compose up` --> To build and run the services, the above `yarn curl` and `yarn web` commands will work
* `docker-compose down` --> Will spin down the container services.

### Suggestions for Improvements
1. Tests.  I banged it out pretty fast and there are NO tests.  Likely one or two ugly bugs even at this early stage.
2. Data is seeded brutally by dumping the current db data and rebuilding according to the .mp3 contents of files/music. Much more intelligence is needed to assure one-to-one relationship between db records and files. Track a file CRC or some such to synchronize.
3. There's repetition and some code ugliness in the db adapters that can be cleaned up.
4. The metadata is parsed from the files such that, if a field is missing on a file, the seeding will go kaboom.  This needs real hardening.
5. Better schema decisions with validation and bullet-proofing all around.  Interfaces, models, validated consistent CRUD results, etc.  TypeScript and/or joi or yup.  Perhaps JSON Schema for Mongo.
6. Cache responses per request.  Redis or whatnot.
7. Request throttling.
8. Obviously, local file storage isn't ideal.  S3 or some cloud service for .mp3 data would be better.
9. Flesh out the rest of the CRUD methods.
10. Better (any) security.