module.exports.checkModel = model => {
  if (!model) {
    throw new Error('Model not present')
  }
  if (!model.collection) {
    throw new Error('No collection on model')
  }
}