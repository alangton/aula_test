const envToAdapter = {
  'mongo': './mongo-adapter',
  'memory': './memory-adapter'
}

const adapterName = process.env.DB_ADAPTER || 'memory'
console.log('Starting adapter: ', adapterName)
module.exports = require(envToAdapter[adapterName])