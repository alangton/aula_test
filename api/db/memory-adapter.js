const R = require('ramda')
const { checkModel } = require('./check-model')

let currentKey = 0
let persisted = {}

const getPersisted = model => {
  if (!persisted[model.collection]) {
    persisted[model.collection] = []
  }
  return persisted[model.collection]
}

const nextKey = () => {
  currentKey += 1
  return `${currentKey}` // use strings
}

module.exports.index = async (model, query = {}, opts = {}) => {
  checkModel(model)

  const allData = getPersisted(model)
  const len = allData.length
  let start = 0
  let end = len

  if (opts.limit || opts.skip) {
    start = opts.skip < len ? opts.skip : len
    end = start + opts.limit < len ? opts.limit : len - start
  }

  const pageCount = Math.ceil(len / opts.limit);
  const sliced = start === len ? [] : allData.slice(start, start + end)
  return {
    pageCount,
    itemCount: sliced.length,
    data: sliced
  }
}


module.exports.findOne = async (model, query = {}, opts = {}) => {
  checkModel(model)
  if (!query._id) {
    throw new Error(`No id on query`)
  }

  const modelData = getPersisted(model)

  const item = modelData.find(item => item._id === query._id)

  return Promise.resolve({
    itemCount: 1,
    data: [
      item
    ]
  })
}

const singleInsert = (item, array) => {
  if (item._id) {
    throw new Error(`Inserting item but it already has an id: ${item._id}`)
  }
  array.push({...item, _id: nextKey()})
}

module.exports.insert = async (model, data, opts = {}) => {
  checkModel(model)

  if (!model.validate(data)) {
    throw new Error(`Data is invalid for ${model.name}`)
  }

  const modelData = getPersisted(model)

  if (R.type(data) === 'Array') {
    for (let item of data) {
      singleInsert(item, modelData)
    }
  } else {
    singleInsert(data, modelData)
  }

  return true

}

module.exports.singleUpdate = (item, array, upsert = false) => {
  if (!item._id) {
    if (!upsert) {
      throw new Error(`Trying to update an item without an id`)
    }
    singleInsert(item, array)
  } else {
    const oldItem = array.find(old => old._id === item._id)
    if (!oldItem) {
      if (!upsert) {
        throw new Error(`Cannot update missing _id: ${item._id}`)
      }
      array.push(item)
    } else {
      array.splice(array.indexOf(oldItem), 1, {...oldItem, ...item})
    }
  }
}

module.exports.update = async (model, data, opts = {}) => {
  checkModel(model)

  if (!model.validate(data)) {
    throw new Error(`Data is invalid for ${model.name}`)
  }

  const modelData = getPersisted(model)

  if (R.type(data) === 'Array') {
    for (let item of data) {
      singleUpdate(item, modelData)
    }
  } else {
    singleUpdate(data, modelData)
  }

  return true
}

module.exports.deleteOne = async (model, query = {}, opts = {}) => {
  checkModel(model)

  const modelData = getPersisted(model)
  const index = modelData.find(item => query._id === item._id)
  persisted = modelData.splice(index, 1)

  return true
}

module.exports.count = async model => {
  checkModel(model)

  return getPersisted(model).length
}

module.exports.clear = () => {
  persisted = {}
  currentKey = 0
}