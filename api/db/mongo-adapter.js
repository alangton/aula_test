const mongodb = require('mongodb')
const R = require('ramda')
const { checkModel } = require('./check-model')

const dbName = 'audiola'
const url = `${process.env.MONGO_URL || 'mongodb://localhost:27017'}/${dbName}`
const client = new mongodb.MongoClient(url)



let db

const getDb = async () => {
  if (!db) {
    const mongo = await client.connect()
    db = mongo.db(dbName)
  }

  return db
}

module.exports.index = async (model, query, opts = {}) => {
  checkModel(model)

  if (!query) {
    query = {} // find all
  }

  const db = await getDb()
  const result = await db.collection(model.collection).find(query, opts).toArray()
  const count = await db.collection(model.collection).count()
  let pageCount = 1

  if (opts.limit && opts.skip) {
    pageCount = Math.ceil(count / opts.limit);
  }

  return {
    pageCount,
    itemCount: result.length,
    data: result
  }
}

module.exports.findOne = async (model, query, opts = {}) => {
  checkModel(model)

  if (!query) {
    throw new Error('Cannot find with an empty query')
  }

  let _id = query._id
  if (_id && R.type(_id) === 'String') {
    _id = mongodb.ObjectID.createFromHexString(_id)
  }

  const useQuery = _id ? {...query, _id } : query

  const db = await getDb()

  const result = db.collection(model.collection).findOne(useQuery, opts)
  return {
    itemCount: 1,
    data: [result]
  }

}

module.exports.insert = async (model, data, opts = {}) => {
  checkModel(model)

  if (!model.validate(data)) {
    throw new Error(`Data is invalid for ${model.name}`)
  }

  const db = await getDb()

  if (typeof data === 'array') {
    return db.collection(model.collection).insertMany(data, opts)
  }

  return db.collection(model.collection).insert(data, opts)

}

module.exports.update = async (model, data, opts = {}) => {
  checkModel(model)

  if (!model.validate(data)) {
    throw new Error(`Data is invalid for ${model.name}`)
  }

  const db = await getDb()

  if (typeof data === 'array') {
    return db.collection(model.collection).updateMany(data, opts)
  }

  return db.collection(model.collection).update(data, opts)
}

module.exports.deleteOne = async (model, query = {}, opts = {}) => {
  checkModel(model)

  const db = await getDb()

  return db.collection(model.collection).delete(query, opts)
}

module.exports.count = async model => {
  checkModel(model)

  const db = await getDb()

  return db.collection(model.collection).count()
}

module.exports.clear = async model => {
  checkModel(model)

  const db = await getDb()

  return db.collection(model.collection).deleteMany({})
}
