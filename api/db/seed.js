const { clear, insert } = require('./index')
const model = require('../songs/model')
const { forceSyncData } = require('../file_handler')

const data = {
  [model.collection]: forceSyncData()
}

const models = [model]

module.exports.seedTheData = async () => {
  // wholly overwrites the database data on startup based on what's in files directory
  for (let model of models) {
    await clear(model)
    const modelData = await data[model.collection]
    await insert(model, modelData)
    console.log(`${model.name} data seeded to db`)
  }

}

