const audioMetaData = require('audio-metadata')
const fs = require('fs')
const path = require('path')
const moment = require('moment')
const { includes } = require('ramda')

const extensions = ['.mp3']

const musicLocation = path.resolve(__dirname, '../files/music')

const fitToShape = (fileName, metaData) => ({
  file: fileName,
  title: metaData.title,
  artist: metaData.artist,
  album: metaData.album,
  year: `${moment(metaData.year).year()}`
})

const getFileMetaData = async fileName => {
  return new Promise((resolve, reject) => {
    fs.readFile(`${musicLocation}/${fileName}`, (err, file) => {
      if (err) {
        return reject(err)
      }
      resolve(fitToShape(fileName, audioMetaData.id3v2(file)))
    })
  })
}

module.exports.forceSyncData = async () => {
  return new Promise((resolve, reject) => {
    fs.readdir(musicLocation, { withFileTypes: true }, (err, files) => {
      if (err) {
        console.error(err)
        return reject(err)
      }

      const musicFiles = files.filter(file => {
        return includes(path.extname(file.name).toLowerCase(), extensions)
      })

      const dataPromises = musicFiles.map(file => getFileMetaData(file.name))

      Promise.all(dataPromises).then(allMetaData => {
        resolve(allMetaData)
      })
    })
  })
}

module.exports.addFile = async fileData => {
  // todo
  return Promise.resolve()
}

module.exports.getFileMetadata = getFileMetaData