const express = require('express')
const R = require('ramda')
const cors = require('cors')
const bodyParser = require('body-parser')
const songRouter = require('./songs/rest')
const { seedTheData } = require('./db/seed')

// REST cors setup
const whitelist = ['http://localhost']
const corsOptions =  {
  origin: (origin, callback) => {
    if (R.includes(origin, whitelist) || !origin) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'))
    }
  }
}

const server = express()

server.use(cors(corsOptions))
server.use(bodyParser.json())
server.use('/api/songs', songRouter)

seedTheData().then(() => {
  server.listen(3000, err => {
    if (err) {
      console.error(err)
      process.exit(1)
    } else {
      process.title = 'aula';
      console.log('Server is up on 3000')
    }
  })
})


