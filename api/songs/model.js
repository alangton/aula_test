const R = require('ramda')

const shape = {
  file: 'string',
  title: 'string',
  artist: 'string',
  album: 'string',
  year: 'string'
}

const validate = song => {
  const validKeys = Object.keys(shape)
  const songKeys = Object.keys(song)
  for (let key of songKeys) {
    if (key === '_id') {
      continue
    }
    if (!R.includes(key, validKeys)) {
      return false
    }
    if (typeof song[key] !== shape[key]) {
      return false
    }
  }
  return true
}

module.exports =  {
  name: 'Song',
  plural: 'Songs',
  collection: 'songs',
  validate: data => {
    if (R.type(data) === 'Array') {
      for (let song of data) {
        if (!validate(song)) {
          return false
        }
      }
      return true
    }
    return validate(data)
  }
}