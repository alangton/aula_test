const R = require('ramda')
const express = require('express')
const paginate = require('express-paginate')
const adapter = require('../db')
const model = require('./model')

const router = express.Router()

router.use(paginate.middleware(3, 50))

router.get('/', async (req, res, next) => {
  const result = await adapter.index(model, {}, { limit: req.query.limit, skip: req.skip })
  res.json(result)
})


router.get('/:id', async (req, res, next) => {
  const id = R.path(['params', 'id'], req)
  if (id) {
    const result = await adapter.findOne(model, {_id: id})
    res.json(result)
  } else {
    next(new Error('No id specified'))
  }
})

module.exports =  router

